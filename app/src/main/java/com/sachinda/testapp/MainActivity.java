package com.sachinda.testapp;

import android.os.Bundle;
import android.util.Log;

import com.sachinda.testapp.models.PostsModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    //Create a sample application to make a service call for the below api using
    // Retrofit, Volley, or Basic HttpUrlConnection. Following Test Driven Design, implement it in the MVP,
    // MVVM, or any other design pattern.
    // API: https://jsonplaceholder.typicode.com/posts
    RecyclerView postsView;
    private MainAdapter mAdapter;
    List<PostsModel> postsArr;
    MainViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        model = ViewModelProviders.of(this).get(MainViewModel.class);
        postsArr = new ArrayList<>();

        postsView = findViewById(R.id.posts_recycler_view);
        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        postsView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MainAdapter(this, postsArr);
        postsView.setAdapter(mAdapter);
        model.observePosts().observe(this, value -> {
            postsArr = value;
            Log.v("Array", postsArr.get(0).title);
            mAdapter.setItems(postsArr);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        model.getPosts();
    }
}
