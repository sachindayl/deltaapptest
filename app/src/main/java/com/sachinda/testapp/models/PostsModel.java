package com.sachinda.testapp.models;

import com.google.gson.annotations.SerializedName;

public class PostsModel {
    public int userId;
    public int id;
    public String title;
    @SerializedName("body")
    public String objectBody;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObjectBody() {
        return objectBody;
    }

    public void setObjectBody(String objectBody) {
        this.objectBody = objectBody;
    }
}
