package com.sachinda.testapp.services;

import com.sachinda.testapp.models.PostsModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostsService {
    public PostsService(){

    }
    private Retrofit buildUrl(){
        return new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public Observable<List<PostsModel>> getPosts() {
        return buildUrl().create(PostsApi.class).getPosts();
    }
}
