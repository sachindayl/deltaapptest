package com.sachinda.testapp.services;

import com.sachinda.testapp.models.PostsModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface PostsApi {

    @GET("posts")
    Observable<List<PostsModel>> getPosts();
}
