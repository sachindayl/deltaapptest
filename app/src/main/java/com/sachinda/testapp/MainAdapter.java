package com.sachinda.testapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sachinda.testapp.models.PostsModel;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<PostsModel> mData;
    private LayoutInflater mInflater;

    // data is passed into the constructor
    MainAdapter(Context context, List<PostsModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }


    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_view_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PostsModel object = mData.get(position);
        holder.objectId.setText(String.valueOf(object.id));
        holder.objectTitle.setText(object.title);
        holder.objectValue.setText(object.objectBody);
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setItems(List<PostsModel> newData) {
        this.mData = newData;
        notifyDataSetChanged();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView objectId;
        TextView objectTitle;
        TextView objectValue;

        ViewHolder(View itemView) {
            super(itemView);
            objectId = itemView.findViewById(R.id.objectId);
            objectTitle = itemView.findViewById(R.id.objectTitle);
            objectValue = itemView.findViewById(R.id.objectValue);
        }
    }

}
