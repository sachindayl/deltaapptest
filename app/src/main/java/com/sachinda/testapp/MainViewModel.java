package com.sachinda.testapp;

import android.util.Log;

import com.sachinda.testapp.models.PostsModel;
import com.sachinda.testapp.services.PostsService;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private MutableLiveData<List<PostsModel>> postsObserver = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<PostsModel> postsArray = new ArrayList<>();

    LiveData<List<PostsModel>> observePosts() {
        return postsObserver;
    }

    void getPosts() {
        PostsService postsService = new PostsService();
        Disposable disposable = postsService.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(value -> {
                            postsArray = value;
                            Log.v("ArrayVM", postsArray.get(0).title);
                            postsObserver.setValue(postsArray);
                        },
                        Throwable::printStackTrace);

        compositeDisposable.add(disposable);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
