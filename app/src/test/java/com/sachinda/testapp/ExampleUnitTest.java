package com.sachinda.testapp;

import com.sachinda.testapp.models.PostsModel;
import com.sachinda.testapp.services.PostsService;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    Observer<List<PostsModel>> observer;
    @Mock
    private PostsService postsService;
    private MainViewModel mainViewModel;
    private List<PostsModel> resultArray;
    private CompositeDisposable compositeDisposable;
    private Disposable disposable;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mainViewModel = new MainViewModel();
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
        compositeDisposable = new CompositeDisposable();
    }

    @Test
    public void check_values_return_array() {
        List<PostsModel> posts = new ArrayList<>();
        resultArray = new ArrayList<>();
        PostsModel p = new PostsModel();
        p.id = 1;
        p.title = "test";
        p.userId = 2;
        p.objectBody = "this is a test body";
        posts.add(p);

        Mockito.when(postsService.getPosts()).thenReturn(Observable.just(posts));
        disposable = postsService.getPosts()
                .subscribe(value -> resultArray = value);
        compositeDisposable.add(disposable);

        assertThat(resultArray, hasSize(1));
        assertEquals(posts, resultArray);

    }

    @Test
    public void check_stream_return() {
        List<PostsModel> posts = new ArrayList<>();
        resultArray = new ArrayList<>();
        PostsModel p = new PostsModel();
        p.id = 1;
        p.title = "test";
        p.userId = 2;
        p.objectBody = "this is a test body";
        posts.add(p);

        Mockito.when(postsService.getPosts()).thenReturn(Observable.just(posts));

        TestObserver<List<PostsModel>> testObserver = postsService.getPosts().test();
        testObserver
                .assertSubscribed()
                .assertValue(posts)
                .assertComplete()
                .assertNoErrors();

        compositeDisposable.add(testObserver);
    }

    @Test
    public void check_stream_empty() {

        Mockito.when(postsService.getPosts()).thenReturn(Observable.empty());

        TestObserver<List<PostsModel>> testObserver = postsService.getPosts().test();
        testObserver
                .assertSubscribed()
                .assertValueCount(0)
                .assertComplete()
                .assertNoErrors();

        compositeDisposable.add(testObserver);

    }

    @Test
    public void check_live_data_changes() {
        MutableLiveData<List<PostsModel>> postsObserver = new MutableLiveData<>();
        List<PostsModel> posts = new ArrayList<>();
        resultArray = new ArrayList<>();
        PostsModel p = new PostsModel();
        p.id = 1;
        p.title = "test";
        p.userId = 2;
        p.objectBody = "this is a test body";
        posts.add(p);

        Mockito.when(postsService.getPosts()).thenReturn(Observable.just(posts));

        disposable = postsService.getPosts()
                .subscribe(postsObserver::setValue);
        compositeDisposable.add(disposable);

        mainViewModel.observePosts().observeForever(observer);
        assertEquals(posts, postsObserver.getValue());

    }

    @After
    public void tearDown() {
        RxAndroidPlugins.reset();
        compositeDisposable.dispose();
        mainViewModel.observePosts().removeObserver(observer);
    }
}